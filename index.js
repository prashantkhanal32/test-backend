const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
const PORT = 5000;

app.use(cors());
app.use(express.json());

mongoose
	.connect(
		"mongodb+srv://prashantkhanal32:sAd8XRC4FlpjyuVq@cluster0.6h7thq2.mongodb.net/?retryWrites=true&w=majority"
	)
	.then(() => console.log("Connected to MongoDB"))
	.catch((err) => console.error("Error connecting to MongoDB:", err));

const userSchema = new mongoose.Schema({
	name: { type: String, required: true },
	email: { type: String, required: true },
	password: { type: String, required: true },
	address: { type: String, required: true },
	phone: { type: String, required: true },
});

const User = mongoose.model("User", userSchema);

app.post("/api/user", (req, res) => {
	const { name, email, password, phone, address } = req.body;

	const newUser = new User({ name, email, password, phone, address });
	newUser
		.save()
		.then((user) => res.json(user))
		.catch((err) =>
			res
				.status(500)
				.json({ message: "Error saving user to the database.", error: err })
		);
});

app.get("/api/users", (req, res) => {
	User.find()
		.then((users) => res.json(users))
		.catch((err) =>
			res.status(500).json({
				message: "Error fetching users from the database.",
				error: err,
			})
		);
});

app.put("/api/user/:userId", (req, res) => {
	const { email, password, phone, address, name } = req.body;

	User.findByIdAndUpdate(
		req.params.userId,
		{ name, email, password, phone, address },
		{ new: true }
	)
		.then((updatedUser) => res.json(updatedUser))
		.catch((err) =>
			res.status(500).json({ message: "Error updating user data.", error: err })
		);
});
app.delete("/api/user/:userId", (req, res) => {
	User.findByIdAndDelete(req.params.userId)
		.then(() => res.json({ message: "User deleted successfully!" }))
		.catch((err) =>
			res.status(500).json({ message: "Error deleting user data.", error: err })
		);
});

app.get("/api/user/:userId", (req, res) => {
	User.findById(req.params.userId)
		.then((user) => {
			if (!user) {
				return res.status(404).json({ message: "User not found." });
			}
			res.json(user);
		})
		.catch((err) =>
			res
				.status(500)
				.json({ message: "Error fetching user from the database.", error: err })
		);
});

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
